"use strict";

import $ from 'jquery';
import Tracker from './tracker-local';
(function (globals) {
  globals.Tracker = Tracker;

	let src = $('script#usario_tracking').attr('src');
	let uuid = src && src.indexOf('uuid=') > 0 ? src.split('uuid=')[1] : null;
	if(uuid) {
		globals.usuarioClient = new Tracker({uuid: uuid});
	}

})(typeof window === 'undefined' ? {} : window);
