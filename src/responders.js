"use strict";

import Cookies from 'cookies-js';
import request from 'superagent';

/**
 * superagent request wrapper for json calls
 * @param url
 * @param verb
 * @param data
 * @param cb
 * @param accepts
 */
const json = function (url, verb, data, cb, accepts) {
  if (typeof accepts == "undefined") {
    accepts = 'application/json'
  }
  if (typeof data == 'function') {
    cb = data;
    data = {};
  }
  request[verb](url)
    .set('Accept', accepts)
    .set('Content-Type', 'application/json')
    .send(data)
    .end(cb);
};

// if cookie doesn't exists or if domain changed, add new cookie otherwise update current
export function createOrUpdateCookie(apiUrl, domainId) {
  let responderCookie;
  try {
    responderCookie = JSON.parse(Cookies.get('usuar.io_responder'));
  } catch (e) {
    responderCookie = null;
  }
  if (responderCookie && responderCookie.domainId === domainId) {
    return new Promise(function(resolve, reject) {
      json(`${apiUrl}/SurveyCookies/${responderCookie.id}`, 'put', {
        lastActivity: new Date()
      }, (err, response) => {
        if(err) {
          return reject(err);
        }

        return resolve(response);
      });
    });
  } else {
    return new Promise(function(resolve, reject) {
      json(`${apiUrl}/SurveyCookies`, 'post', {
        domainId: domainId
      }, (err, response) => {
        if(err) {
          reject(err);
        }

        responderCookie = response.body;
        Cookies.set('usuar.io_responder', JSON.stringify(responderCookie), {
          expires: 60 * 60 * 24 * 7
        });

        resolve(response);
      });
    });
  }
}

// update responder cookie  last survey seen time and surveys view count
export function updateViewCount(apiUrl, responderCookie) {
  json(`${apiUrl}/SurveyCookies/${responderCookie.id}`, 'put', {
    surveysSeen: ++responderCookie.surveysSeen,
    lastSurveySeen: new Date()
  }, (err, response) => {
    Cookies.set('usuar.io_responder', JSON.stringify(responderCookie), {expires: 60 * 60 * 24 * 7});
  });
}

// update responder cookie  last survey answer time and surveys answered count
export function updateAnswerCount(apiUrl, responderCookie) {
  json(`${apiUrl}/SurveyCookies/${responderCookie.id}`, 'put', {
    lastSurveyResponse: new Date(),
    surveysAnswered: ++responderCookie.surveysAnswered
  }, (err, response) => {
    Cookies.set('usuar.io_responder', JSON.stringify(responderCookie), {expires: 60 * 60 * 24 * 7});
  });
}
