"use strict";

import $ from 'jquery';
import request from 'superagent';
import Cookies from 'cookies-js';
import {getDeviceType, isReturningUser, timesVisited, getCookie} from './utils';
import {deviceFilter, ipFilter, sourceFilter} from './filters';
import * as responder from './responders';
/**
 * superagent request wrapper for json calls
 * @param url
 * @param verb
 * @param data
 * @param cb
 * @param accepts
 */
const json = function (url, verb, data, cb, accepts) {
  if (typeof accepts == "undefined") {
    accepts = 'application/json'
  }
  if (typeof data == 'function') {
    cb = data;
    data = {};
  }
  request[verb](url)
    .set('Accept', accepts)
    .set('Content-Type', 'application/json')
    .send(data)
    .end(cb);
};

export default class Tracker {
  constructor(opts, global) {
    this.apiUrl = 'http://localhost:3000/api';
    this.cookieName = 'usuario-ck';
    this.isSending = false; //flag is survey is sending response
    //TODO: set current device type [Mobile, Tablet, Desktop]
    this.window = typeof window === "undefined" ? global : window;
    this.deviceType = this.getDeviceType();
    this.trafficSource = 'Default'; //TODO: get traffic from url
    this.clientIP = '';
    let defaults = {};
    //override defaults with provided opts
    this.opts = $.extend(defaults, opts);
    this.surveyFilters = [];
    this.parameters = {};
    this.uiidCheckUrl = `${this.apiUrl}/tracker/uuid/${this.opts.uuid}`;
    this.getSurveyUrl = `${this.apiUrl}/get-survey`;
    this.language = this.window.navigator.userLanguage || this.window.navigator.language;
    this.returningUser = false;
    this.surveyMinimized = false;

    //check if the user is new or returning
    this.isReturningUser = isReturningUser();
    this.timesVisited = timesVisited();
    this.connectToApi();
  }

  connectToApi() {
    //query UUID to get the survey,domain and organizationId for this UUID
    json(this.uiidCheckUrl, 'get', (err, response)=> {
      if (err) {
      }
      const data = response.body;
      //UUID exists
      if (data.exists) {
        //this.opts.surveyId = data.surveyId;
        this.opts.countryCode = data.countryCode;
        this.opts.organizationId = data.organizationId;
        this.opts.domainId = data.domainId;
        this.parameters = $.extend(this.parameters, data.parameters);
        this.surveyFilters = this.surveyFilters.concat(data.surveyFilters);
        //make the urls
        //this.idSelector = `survey-${this.opts.surveyId}-${this.opts.domainId}-id`;
        this.sendResponseUrl = `${this.apiUrl}/tracker`;
        //this.clientIP = data.clientIP ;
        json(this.getSurveyUrl, 'post', {
          source: this.trafficSource,
          device: this.deviceType,
          pageTarget: this.window.location.href,
          uuid: this.opts.uuid,
          domainId: data.domainId,
          organizationId: data.organizationId,
          countryCode: this.opts.countryCode
        }, (err, response) => {
          if (err) {
          }
          if (response.body) {
            console.log(response.body);
            //obtain view and response count from survey
            this.opts.cursorTrigger = response.body.cursorTrigger || false;
            this.opts.maxAnswers = response.body.maxAnswers || 0;
            this.opts.maxViews = response.body.maxViews || 0;
            this.viewCount = response.body.viewCount || 0;
            this.TTO = response.body.inPageTime;
            this.numberOfPages = response.body.numberOfPages;
            this.responseCount = response.body.responseCount || 0;
            this.opts.surveyId = response.body.id;
            this.opts.surveyName = response.body.name;
            this.questions = response.body.questions;
            this.questLength = response.body.questions.length;
            this.filters = response.body.filters;
            this.templateUrl = `${this.apiUrl}/${this.opts.organizationId}/${response.body.domainId}/${response.body.id}/template`;
            this.surveyPosition = response.body.parameters.position ? response.body.parameters.position : 'right';
            this.skipFilters = response.body.skipFilters || false;
            //track survey calls (intents to render the survey)
            json(`${this.apiUrl}/Surveys/${this.opts.surveyId}/updateVisitCount`, 'post', (err, response) => {
            });

            // update responder survey seen count
            responder.createOrUpdateCookie(this.apiUrl, response.body.domainId)
              .then((response) => {
                this.responderCookie = response.body;
                responder.updateViewCount(this.apiUrl, response.body);
              })
              .catch((err) => {
                console.log('err', err);
              });

            this.renderTemplate();
          } else {
            responder.createOrUpdateCookie(this.apiUrl, this.opts.domainId);
          }
        });

        //get all survey filters
      } else {
        //UUID is incorrect or doesn't exist
      }
    });
  }

  sendResponse() {
    if (this.isSending) {
      return;
    }
    this.isSending = true;
    let payload = {};
    payload.domainId = this.opts.domainId;
    payload.surveyId = this.opts.surveyId;
    //current page url
    payload.referrer = this.window.location.href; //referrer url (ie. Traffic source tracking)
    payload.response = this.responseReady;
    payload.timeToResponse = this.timeToResponse ? this.timeToResponse : 0;
    console.log(payload.timeToResponse);
    json(this.sendResponseUrl, 'post', payload, (err, response)=> {
      this.isSending = false;
      console.log('Send response');

      // send google analytics
      if(this.opts.analytics && ga) {
        let surveyName = `${this.opts.surveyId} : ${this.opts.surveyName}`;
        let responseId = response.body.id;

        ga((domainAnalytics) => {
          if(!domainAnalytics){
            return;
          }

          let trackingId = domainAnalytics.get('trackingId');
          if(!/(UA-[0-9]{3,}-[0,9]{1,2})|(UA-[0-9]+-[0-9]{1,3})/i.test(trackingId)) {
            return;
          }

          ga('create', trackingId, 'auto', {'name': 'usario'});
          ga('usario.send', {
            'hitType': 'event',
            'eventCategory': 'Usario Surveys',
            'eventAction': surveyName,
            'eventLabel': responseId
          });
          console.log('Send analytics');
        });
      }
    });
    // update responder survey answer count
    responder.updateAnswerCount(this.apiUrl, this.responderCookie);
  }

  processFilters() {
    if(this.opts.maxAnswers > 0 && this.responseCount >= this.opts.maxAnswers) {
      return false;
    }
    if (this.opts.maxViews > 0 && this.viewCount >= this.opts.maxViews) {
      return false;
    }
    let conditionsToTest = [];
    //lets act on some filters
    this.filters.forEach(filter => {//OR level grouping
      let conditionGroupAND = true;
      filter.group.rules.forEach(filterRule => {//AND level grouping
        if (!!filterRule.condition) {
          if (filterRule.filterType == "LANG") {
            conditionGroupAND = conditionGroupAND && (filterRule.condition.toLowerCase() === this.language.toLowerCase());
          }
          if (filterRule.filterType == "VIS") {
            if (filterRule.condition === "New" && !this.isReturningUser) {
              conditionGroupAND = true;
            } else if (filterRule.condition === "Returning" && this.isReturningUser) {
              conditionGroupAND = true;
            } else {
              conditionGroupAND = false;
            }
          }
          if (filterRule.filterType == "COOKIE") {
            let cookie = getCookie(filterRule.condition.cookieName);
            conditionGroupAND = cookie === filterRule.condition.cookieValue;
          }
        } else {
          conditionGroupAND = false;
        }
      });
      conditionsToTest.push(conditionGroupAND);
    });
    const resolved = conditionsToTest.reduce((acc, testA) => acc || testA, false); //use OR to test
    if (!resolved) {
      return false;
    }

    if (this.TTO && this.TTO.enabled) {
      if (this.TTO.format == 's') {
        this.TTOperiod = this.TTO.period * 1000;
      } else if (this.TTO.format == 'm') {
        this.TTOperiod = this.TTO.period * 60 * 1000;
      }
    }

    if (this.numberOfPages && this.numberOfPages.enabled) {
      if (this.timesVisited <= this.numberOfPages.quantity) return false;
    }

    return true;
  }

  renderTemplate() {
    //if views/answers limit is reached we don't even make the http call.
    //TODO: not show if option checked
    if (this.processFilters() && !this.skipFilters) {
      this.showSurvey();
    }

  }

  showSurvey() {
    setTimeout(() => {
      json(this.templateUrl, 'get', (err, response) => {
        if (err) {
        }

        if (this.opts.cursorTrigger) { // is mouse is out option is selected then only trigger survey when is out of window
          $(document).mouseleave(() => {
            this.surveyBuilder(response, this.surveyPosition);
            $(document).unbind('mouseleave');
          });
        } else { // regular flow is out mouse option is not selected.
          this.surveyBuilder(response, this.surveyPosition);
        }
      }, 'text/html');
    }, this.TTOperiod || 10);
  }

  show(id) {
    //survey init wrapped in timeout so is async loaded
    if (this.skipFilters) {
      if (id) { //if id passed show that survey id only if belongs to user and domain
        this.opts.surveyId = id;
        this.templateUrl = `${this.apiUrl}/${this.opts.organizationId}/${this.opts.domainId}/${id}/template`;
      }
      setTimeout(() => {
        json(this.templateUrl, 'get', (err, response) => {
          if (err)
            this.surveyBuilder(response, this.surveyPosition);
          //displaySurvey();
        }, 'text/html');
      }, 10);
    }
  }

  surveyBuilder(response, renderPosition) {
    //return () => {
    let startTime = new Date();
    let endTime;
    json(`${this.apiUrl}/Surveys/${this.opts.surveyId}/updateViewCount`, 'post', (err, response) => {
    });
    //set cookies value if passed
    if (this.parameters.COOKIE) {
      Cookies.set(this.cookieName, this.parameters.COOKIE.value, {expires: 600}); //10 min.
    }
    const data = response.text;
    let $template = $('<div>');
    $template
      .addClass('usuario-wrapper')
      .addClass(renderPosition)
      .html(data)
      .appendTo('body');

    // not supported in IE will be removed soon
    // let shadowDiv;
    // let template = $template.get(0);
    //
    // if ('attachShadow' in template) {
    //   shadowDiv = template.attachShadow({ mode: 'closed' });
    // } else {
    //   shadowDiv = template.createShadowRoot();
    // }
    // shadowDiv.innerHTML = template.outerHTML;
    // let $div = $(shadowDiv).find('.usuario-wrapper');
    // ---end---

    let $div = $template;
    this.opts.analytics = $div.find('form.usuario-survey').data('analytics');
    this.questLength = $div.find('.step').length;
    this.surveyShowing = true;
    let step = $div.find('.step');
    let sendBtn = $div.find('.send');
    let nextBtn = $div.find('.next');
    let infoBtn = $div.find('.info-btn');
    let usarioLink = $div.find('.usario-link');
    let replyBtn = $div.find('.reply');
    let radios = $div.find('.form-input-radio');
    let shortTextInput = $div.find('input.form-input-text');
    let emoticonResponse = $div.find('.input-emotion');
    let netPromoterScore = $div.find('.input-scale');
    let surveyHandle = $div.find('.usuario-survey-handle').addClass('usuario-survey-handle-open');
    let step0 = $div.find('.step').not('.step-0').hide();
    let thankYouMsg = $div.find('.step-final').hide();

    //position the survey bellow the fold
    $div.css({bottom: -($div.height())});
    surveyHandle.removeClass('usuario-survey-handle-open').addClass('usuario-survey-handle-close');
    this.surveyShowing = false;
    if(this.getDeviceType() != 'Desktop'){
      $div.find('.hide-mobile').hide();
      $div.find('.show-mobile').show();
    }else{
      $div.find('.show-mobile').hide();
    }

    radios.on('change', function (e) {
      for (var i = 0; i < radios.length; i++) {
        var radio = radios[i];
        var grandParent = radio.parentNode.parentNode;
        if(radio.checked){
          // if node already has className 'selected' doesn't add it again
          grandParent.className = grandParent.className.indexOf('selected') < 0 ? grandParent.className + ' selected' : grandParent.className;
        }else{
          grandParent.className = grandParent.className.replace(' selected', '');
        }
      }
    });

    // prevent enter form submit to avoid multiple button appearance
    shortTextInput.on('keydown', event => {
      if(event.which === 13) {
        event.preventDefault();
      }
    });

    $div.animate({bottom: 0});
    this.surveyShowing = true;
    surveyHandle.removeClass('usuario-survey-handle-close').addClass('usuario-survey-handle-open');

    replyBtn.click(event => {
      event.preventDefault();
      $div.find('.show-mobile').hide();
      $div.find('.hide-mobile').show();
    });

    //Logic operations for next button to iterate through questions.
    this.questIteration = 0;
    (this.questLength == 1) ? nextBtn.hide() : (nextBtn.show() && sendBtn.hide());
    // hide next and send buttons if question type is emoticonResponse or netPromotorScore
    this.hideShowButtons(this.questIteration, this.questions, nextBtn, sendBtn, infoBtn, usarioLink);

    nextBtn.click(event => {
      event.preventDefault();
      const currentQuest = step.eq(this.questIteration);
      this.next(currentQuest, step, this.questions, nextBtn, sendBtn, infoBtn, usarioLink);
    });

    infoBtn.click(event => {
      event.preventDefault();
      const currentQuest = step.eq(this.questIteration);
      this.next(currentQuest, step, this.questions, nextBtn, sendBtn, infoBtn, usarioLink);
    });

    sendBtn.click(event => {
      event.preventDefault();
      endTime = new Date();
      this.timeToResponse = endTime - startTime;
      //To hide survey when send is pressed.
      this.responseReady = $div.find('form').serializeArray();
      $div.find('.step').hide();
      $div.find('.form-buttons').hide();
      thankYouMsg.show();
      this.sendResponse();
      setTimeout(()=> {
        $div.find('.usuario-survey-handle').hide();
        $div.find('.form-questions').hide();
        $div.find('.form-footer').hide();
      }, 4 * 1000);

    });

    surveyHandle.click(event => {
      event.preventDefault();
      if (this.surveyShowing) {
        $div.animate({bottom: -($div.height())});
        surveyHandle.removeClass('usuario-survey-handle-open').addClass('usuario-survey-handle-close');
        this.surveyShowing = false;
      } else {
        $div.animate({bottom: 0});
        this.surveyShowing = true;
        surveyHandle.removeClass('usuario-survey-handle-close').addClass('usuario-survey-handle-open');
      }
      if(surveyHandle.hasClass('usuario-survey-handle-close') && !this.surveyMinimized){
        json(`${this.apiUrl}/Surveys/${this.opts.surveyId}/updateMinimizeCount`, 'post', (err, response) => {
          this.surveyMinimized = true;
        });
      }
    });

    // submit survey on emoticonResponse select, if it's last question otherwise move to next question
    emoticonResponse.on('change', () => {
      if(this.questIteration === (this.questLength - 1)) {
        setTimeout(() => {
          sendBtn.trigger('click');
        }, 500);
      } else {
        setTimeout(() => {
          nextBtn.trigger('click');
        }, 500);
      }
    });

    // submit survey on netPromoterScore select, if it's last question otherwise move to next question
    netPromoterScore.on('change', () => {
      if(this.questIteration === (this.questLength - 1)) {
        setTimeout(() => {
          sendBtn.trigger('click');
        }, 500);
      } else {
        setTimeout(() => {
          nextBtn.trigger('click');
        }, 500);
      }
    });
    //}
  }

  next(currentQuest, step, questions, nextBtn, sendBtn, infoBtn, usarioLink) {
    let questIteration = ++this.questIteration;

    if(questIteration === questions.length) {
      sendBtn.trigger('click');
      return;
    }

    let nextQuestType = questions[questIteration].type;

    // if next question is nested check if matches condition
    if(nextQuestType && nextQuestType.type === 'nested') {
      let nextQuest = step.eq(questIteration);

      // get the if data comes as comma delimited array
      let qIf = nextQuest.data('if');
      qIf = qIf.split(",");

      // get the parent question index
      let qTo = nextQuest.data('to');

      // using a generic lookup for checked input, cause only selection based question are nestable
      let selectedAnswer = step.eq(qTo).find('input:checked').data('letter');
      let matchesCondition = false;

      if (!!selectedAnswer) { //coerce the value to its boolean sexy self
        //check if any of the conditions matches the answer selected
        matchesCondition = qIf.some((current) => {
          return current == selectedAnswer;
        });
      }

      // if doesn't match condition call next() function for next question
      if(matchesCondition) {
        currentQuest.hide(); // hide currentQuest after all login finishes to avoid unwanted behaviour
        step.eq(questIteration).show(); // show next question
        this.hideShowButtons(questIteration, questions, nextBtn, sendBtn, infoBtn, usarioLink);
      } else {
        // if doesn't match condition skip question and go to next question
        this.next(currentQuest, step, questions, nextBtn, sendBtn, infoBtn, usarioLink);
      }
    } else {
      currentQuest.hide(); // hide currentQuest after all login finishes to avoid unwanted behaviour
      step.eq(questIteration).show(); // show next question
      this.hideShowButtons(questIteration, questions, nextBtn, sendBtn, infoBtn, usarioLink);
    }
  }

  hideShowButtons(questIteration, questions, nextBtn, sendBtn, infoBtn, usarioLink) {
    // decide whether to show the send or next button based on the question index.
    (questIteration == (questions.length -1)) ? (nextBtn.hide() && sendBtn.show()) : nextBtn.show();
    
    // move usuario link to next line if button size is big
    usarioLink.insertBefore(infoBtn.parent());
    infoBtn.parent().addClass('form-half');
    infoBtn.hide();

    let nextQuestionInputType = questions[questIteration].inputType.type;
    if(nextQuestionInputType === 'emoticonResponse' || nextQuestionInputType === 'netPromotorScore') {
      // hide next and send button if question type is emoticon or netpromoterscore
      nextBtn.hide();
      sendBtn.hide();
    } else if(nextQuestionInputType === 'informational') {
      // hide next and send button if question type is informational and show custom informational button
        let infoBtnText = questions[questIteration].button;

        nextBtn.hide();
        sendBtn.hide();

        // if button text is large move link on next line after button
        if(infoBtnText.length >= 10) {
          usarioLink.insertAfter(infoBtn.parent());
          infoBtn.parent().removeClass('form-half');
        }
        infoBtn.html(infoBtnText);
        infoBtn.show();
    }

  }

  getDeviceType() {
    return getDeviceType(this.window.navigator.userAgent);
  }
}
