var webpack = require('webpack');
var path = require('path');

module.exports = {
  entry: {
    'client.local': './src/main-local.js',
    'client.test': './src/main-test.js',
    'client': './src/main.js',
  },
  output: {
    path: './lib',
    filename: '[name].min.js'
  },
  module: {
    loaders: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loaders: ['babel-loader']
      }
    ]
  }
};